# [`sempernow/gitlab-workflow`](https://gitlab.com/sempernow/gitlab-workflow)

```bash
# Initialize
git init --initial-branch=main
git add .
git commit -C "Init @ $(date -u '+%Y-%m-%dT%H:%M:%SZ')"
# Add origin : SSH mode 
git remote add origin git@gitlab.com:$(git config user.name)/gitlab-workflow.git
# SSH login 
ssh -T -i ~/.ssh/gitlab_$(git config user.name) git@gitlab.com
# Push
git push origin master
```
